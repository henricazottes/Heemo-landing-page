var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  let locales = req.headers["accept-language"].split(',')
  console.log("locales:", req.headers, locales)
  let locale = locales[0]
  req.i18n.setLocale(req, locale)
  res.redirect('/' + req.i18n.getLocale());
});

/* GET home page. */
router.get('/:lang', function(req, res, next) {
  if(req.params.lang.length > 2) {
    res.redirect('/' + req.i18n.getLocale())
  }
  req.setLocale(req, req.params.lang)
  req.i18n.setLocale(req.params.lang)
  res.render('index');
});

/* GET thanks page. */
router.post('/:lang/thanks', function(req, res, next) {
  req.i18n.setLocale(req, req.params.lang)  
  if(req.body.name !== '') {
    return
  }
  const email = req.body.email.toLowerCase().replace(/\s/g, '');
  const owner = req.body.status === "owner";

  req.db.collection('users').findOne({ email: req.body.email }, function(err, user) {
    if(!user) {
      req.db.collection('users').save({
          email,
          owner,
          created_at: new Date()
        }, (err, result) => {
        if (err) return console.log(err)
        console.log('email saved to database: ', email)
        res.render('thanks', { message: req.__('thanks') });
      });
    } else {
      res.render('thanks', { message: req.__('already_registered') });      
    }
  });
});

module.exports = router;
