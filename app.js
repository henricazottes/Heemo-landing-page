var createError = require('http-errors');
var express = require('express');
var i18n = require("i18n");
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var MongoClient = require('mongodb').MongoClient;

i18n.configure({
  locales:['en', 'fr'],
  directory: __dirname + '/locales'
});

var db

MongoClient.connect(process.env.MONGODB_URI, ({ useNewUrlParser: true }), (err, client) => {
  if (err) return console.log(err)
  const elements = process.env.MONGODB_URI.split('/');
  db = client.db(elements[elements.length-1])
})

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(i18n.init);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req,res,next){
  req.db = db;
  req.i18n = i18n;
  next();
});
app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  console.log('Error ', err)
  res.redirect(`/${req.i18n.getLocale()}`)
});

module.exports = app;
